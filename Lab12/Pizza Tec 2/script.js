//Main Menu (scrolling)
//Buttons Menus
$(document).ready(function(){
	$("a[href='#go_menu']").click(function() {
    var offset = 20; //Offset of 20px
    $('html, body').animate({
        scrollTop: $("#Menu").offset().top + offset
    }, 1000);
	});
	
	$("a[href='#go_find_us']").click(function() {
    var offset = 20; //Offset of 20px
    $('html, body').animate({
        scrollTop: $("#Find_us").offset().top + offset
    }, 1000);
	});
	
	$("a[href='#go_opening_hours']").click(function() {
    var offset = 20; //Offset of 20px
   $('html, body').animate({
	scrollTop: $("#Opening_hours").offset().top + offset
	}, 1000);
    });
        
    $("a[href='#go_contact']").click(function() {
    var offset = 20; //Offset of 20px
   $('html, body').animate({
	scrollTop: $("#contact").offset().top + offset
	}, 1000);
	});

//Menu
    $('#pizza_show').hide();
    $('#pasta_show').hide();
    $('#salad_show').hide();
    $('#drink_show').hide();
    $('#dessert_show').hide();

    $('#pizza_button').on('click',function(){
        $('#pasta_show').slideUp("slow");
        $('#salad_show').slideUp("slow");
        $('#drink_show').slideUp("slow");
        $('#dessert_show').slideUp("slow");
        
        $('#pizza_show').slideToggle("slow");
        
        });

    $('#pasta_button').on('click',function(){
        $('#pizza_show').slideUp("slow");
        $('#salad_show').slideUp("slow");
        $('#drink_show').slideUp("slow");
        $('#dessert_show').slideUp("slow");
        
        $('#pasta_show').slideToggle("slow");
        });

    $('#salad_button').on('click',function(){
        $('#pizza_show').slideUp("slow");
        $('#pasta_show').slideUp("slow");
        $('#drink_show').slideUp("slow");
        $('#dessert_show').slideUp("slow");
        
        $('#salad_show').slideToggle("slow");
        });

    $('#drink_button').on('click',function(){
        $('#pizza_show').slideUp("slow");
        $('#pasta_show').slideUp("slow");
        $('#salad_show').slideUp("slow");
        $('#dessert_show').slideUp("slow");
        
        $('#drink_show').slideToggle("slow");
        });

    $('#dessert_button').on('click',function(){
        $('#pizza_show').slideUp("slow");
        $('#pasta_show').slideUp("slow");
        $('#salad_show').slideUp("slow");
        $('#drink_show').slideUp("slow");
        
        $('#dessert_show').slideToggle("slow");
        });
});


var myCenter=new google.maps.LatLng(20.613118, -100.401042);

function initialize()
{
		var myLatLng={lat: 20.613198, lng: -100.399538};
		var mapProp = 
		{
			center:new google.maps.LatLng(myLatLng),
			zoom:15,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  icon:'images/logomaps.png'
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
src="//maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap"

//Contact form
function get_submit(){

    var name =  document.getElementById('name').value;
    var email =  document.getElementById('email').value;
    var object =  document.getElementById('object').value;
    var message =  document.getElementById('message').value;
    
    if (name == null || name == "" || email == null || email == "" || object == null || object == "" || message == null || message == "") {
        alert("Todos los campos deben ser llenados / All the fields must be filled in");
        return false;
    }
    
}