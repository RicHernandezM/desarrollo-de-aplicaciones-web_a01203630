<?php
function connectMysql(){

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "pizzatec";

$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}    
/*
 //create database
if (!$conn->query("CREATE database IF NOT EXISTS ". $dbname)){
    echo "Error creating database: " . $conn->error;
}*/
 //connect the database
if (!$conn->query("USE ". $dbname)){
    echo "Error using database: " . $conn->error;
}
/*
$sql = "CREATE TABLE IF NOT EXISTS profil 
(
    Email varchar(100) NOT NULL PRIMARY KEY,
    Name varchar(30) NOT NULL,
    Surname varchar(30) NOT NULL,
    Number varchar(20) NOT NULL,
    Username varchar(30) NOT NULL,
    Password varchar(30) NOT NULL,
)";
if(!$conn->query($sql)){
    echo "Table creation failed: (" . $sql . ") " . $conn->error;
}   */ 
    
return $conn;
}