<html lang="en">
   <head>
      <meta charset="utf-8">
      <link rel="stylesheet" href="style.css" >
      <script src="jquery-3.1.0.min.js"></script>
      <script src="http://maps.googleapis.com/maps/api/js"></script>
      <script src="script.js"></script>
      <title>Pizza Tec Querétaro</title>
      <link rel="shortcut icon" type="image/x-icon" href="images/logo.png" />
   </head>
   <body>
      <header>
         <p class="language">
            <a href="Pizza_Tec_EN.php">EN</a>
            <a href="Pizza_Tec_ES.php">ES</a>
         </p>
          
         <p class="connect">
            <?php  session_start();
                include 'login.php'; ?>
             <a href="#"> Hola, <?php echo $_SESSION["username"]; ?> </a>
             <br>
             <a target="_blank" href="db/logout_ES.php">Desloguearse</a>
          </p>
          <section class="Navigation">
            <img  id="logo_menu" src="images/logo2.png" alt="logo"/><br />
            <div id="container">				
               <a href="#go_menu">Menú</a>
               <a href="#go_find_us">Dirección</a> 
               <a href="#go_opening_hours">Horario de apertura</a>
               <a href="#go_contact">Contacto</a>
            </div>
         </section>
      </header>
      <section id="Menu_section">
         <div class="trait_dessus"></div>
         <h1 id="Menu">Menu</h1>
         <h3>Haga clic en cada icono para mostrar el menú</h3>
         <p id="icone">
            <img id="pizza_button" alt ="Pizza" src="images/pizza-3.png"/>
            <img id="pasta_button" alt ="Pasta"  src="images/spaguetti-2.png"/>
            <img id="salad_button" alt ="Salad"  src="images/salad.png"/>
            <img id="drink_button" alt ="Drinks"  src="images/beer.png"/>
            <img id="dessert_button" alt ="Desserts"  src="images/muffin.png"/>
         <div id="pizza_show">
            <h3>Pizzas</h3>
            <table>
               <thead>
                  <tr>
                      <th></th>
                      <th>Ingrediente</th>
                      <th>Cantidad</th>
                      <th>Grande</th>
                      <th>Cantidad</th>
                      <th>Personal</th>
                  </tr>
               </thead>
               <tbody>
                   <tr>
                     <td class="name">Margherita</td>
                     <td class="composition">Jitomate y queso</td>
                      <td><input type="number" name="quantity" id="qt_marguerita_G" min="0" style="width:40px;"/></td>
                     <td class="price">$100</td>
                      <td><input type="number" name="quantity" id="qt_marguerita_P" min="0" style="width:40px;"/></td>
                     
                      <td class="price">$50</td>          
                  </tr>

                  <tr>
                     <td class="name">Funghi</td>
                     <td class="composition">Jamón y champiñones</td>
                      <td><input type="number" name="quantity" id="qt_funghi_G" min="0" style="width:40px;"/></td>
                     <td class="price">$120</td>
                      <td><input type="number" name="quantity" id="qt_funghi_P" min="0" style="width:40px;"/></td>
                     <td class="price">$60</td>
                  </tr>

                  <tr>
                     <td class="name">Hawaiana</td>
                     <td class="composition">Jamón y piña</td>
                      <td><input type="number" name="quantity" id="qt_hawaiana_G" min="0" style="width:40px;"/></td>
                     <td class="price">$120</td>
                       <td><input type="number" name="quantity" id="qt_hawaiana_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$60</td>
                  </tr>

                  <tr>
                     <td class="name">Napoletana</td>
                     <td class="composition">Anchoas y alcaparras</td>
                      <td><input type="number" name="quantity" id="qt_napoletana_G" min="0" style="width:40px;"/></td>
                     <td class="price">$120</td>
                       <td><input type="number" name="quantity" id="qt_napoletana_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$60</td>
                  </tr>

                  <tr>
                     <td class="name">Diavola</td>
                     <td class="composition">Pepperoni y chile</td>
                      <td><input type="number" name="quantity" id="qt_diavola_G" min="0" style="width:40px;"/></td>
                     <td class="price">$125</td>
                       <td><input type="number" name="quantity" id="qt_diavola_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$65</td>
                  </tr>

                  <tr>
                     <td class="name">Salsicha</td>
                     <td class="composition">Oso berlin con pesto o chimichurri</td>
                      <td><input type="number" name="quantity" id="qt_salsicha_G" min="0" style="width:40px;"/></td>
                     <td class="price">$135</td>
                       <td><input type="number" name="quantity" id="qt_salsicha_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$70</td>
                  </tr>

                  <tr>
                     <td class="name">4 Stagioni</td>
                     <td class="composition">Champiñones, jamón, aceitunas y alcachofas</td>
                      <td><input type="number" name="quantity" id="qt_stagioni_G" min="0" style="width:40px;"/></td>
                     <td class="price">$140</td>
                       <td><input type="number" name="quantity" id="qt_stagioni_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$70</td>
                  </tr>

                  <tr>
                     <td class="name">Gamberi</td>
                     <td class="composition">Camarón sazonado</td>
                      <td><input type="number" name="quantity" id="qt_gamberi_G" min="0" style="width:40px;"/></td>
                     <td class="price">$140</td>
                       <td><input type="number" name="quantity" id="qt_gamberi_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$70</td>
                  </tr>

                  <tr>
                     <td class="name">Principessa</td>
                     <td class="composition">Jamón serrano y queso crema</td>
                      <td><input type="number" name="quantity" id="qt_principessa_G" min="0" style="width:40px;"/></td>
                     <td class="price">$145</td>
                       <td><input type="number" name="quantity" id="qt_principessa_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>

                  <tr>
                     <td class="name">4 Fromaggi</td>
                     <td class="composition">Selección de quesos</td>
                      <td><input type="number" name="quantity" id="qt_fromaggi_G" min="0" style="width:40px;"/></td>
                     <td class="price">$145</td>
                       <td><input type="number" name="quantity" id="qt_fromaggi_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div id="pasta_show">
            <h3>Pastas</h3>
            <table>
                <thead>
                <tr>
                      <th></th>
                      <th>Ingrediente</th>
                      <th>Cantidad</th>
                      <th>Precio</th>
                 </tr>
                </thead>
                <tbody>
                     <td class="name">Burro</td>
                     <td class="composition">Mantequilla y parmesano</td>
                       <td><input type="number" name="quantity" id="qt_burro" min="0" style="width:40px;"/></td> 
                     <td class="price">$55</td>
                  </tr>

                  <tr>
                     <td class="name">Pesto</td>
                     <td class="composition">Salsa genovesa de albahaca</td>
                       <td><input type="number" name="quantity" id="qt_pesto" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>

                  <tr>
                     <td class="name">Crema e funghi</td>
                     <td class="composition">Crema y champiñones</td>
                       <td><input type="number" name="quantity" id="qt_cremafunghi" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>

                  <tr>
                     <td class="name">Aglio olio y peperoncino</td>
                     <td class="composition">Ajo aceite de olivo y chile</td>
                       <td><input type="number" name="quantity" id="qt_aglio" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>

                  <tr>
                     <td class="name">Ragú</td>
                     <td class="composition">Carne molida y salsa de tomate</td>
                       <td><input type="number" name="quantity" id="qt_ragu" min="0" style="width:40px;"/></td> 
                     <td class="price">$75</td>
                  </tr>

                  <tr>
                     <td class="name">Gamberi</td>
                     <td class="composition">Camarón</td>
                       <td><input type="number" name="quantity" id="qt_gamberip" min="0" style="width:40px;"/></td> 
                     <td class="price">$80</td>
                  </tr>

                  <tr>
                     <td class="name">Lasagna</td>
                     <td class="composition">"Clasica"</td>
                       <td><input type="number" name="quantity" id="qt_lasagna" min="0" style="width:40px;"/></td> 
                     <td class="price">$55</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div id="salad_show">
            <h3>Insalate</h3>
            <table>
               <thead>
                  <tr>
                     <th></th>
                     <th>Ingrediente</th>
                     <th>Cantidad</th>
                     <th>Precio</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="name">Lechuga jitomate queso crema</td>
                     <td class="composition">Queso azul o cabra y nueces</td>
                       <td><input type="number" name="quantity" id="qt_ltcc" min="0" style="width:40px;"/></td> 
                     <td class="price">$60</td>
                  </tr>

                  <tr>
                     <td class="name">Lechuga jitomate aceitunas</td>
                     <td class="composition">Alcachofa y jamon serrano</td>
                       <td><input type="number" name="quantity" id="qt_lto" min="0" style="width:40px;"/></td> 
                     <td class="price">$65</td>
                  </tr>

                  <tr>
                     <td class="name">Capresse</td>
                     <td class="composition">Jitomate fresco y mozzarella</td>
                       <td><input type="number" name="quantity" id="qt_capresse" min="0" style="width:40px;"/></td> 
                     <td class="price">$65</td>
                  </tr>

                  <tr>
                     <td class="name">Gamberi Biagio</td>
                     <td class="composition">(8 pz. mediano) la receta secreta</td>
                       <td><input type="number" name="quantity" id="qt_gamberib" min="0" style="width:40px;"/></td> 
                     <td class="price">$90</td>
                  </tr>

                  <tr>
                     <td class="name">Langosta Alguer</td>
                     <td class="composition">(Sobre pedido) porción individual de 360 gr.</td>
                       <td><input type="number" name="quantity" id="qt_lobster1" min="0" style="width:40px;"/></td> 
                     <td class="price">$550</td>
                  </tr>

                  <tr>
                     <td class="name">Para cuatro personas</td>
                     <td class="composition">De 1,450 gr</td>
                       <td><input type="number" name="quantity" id="qt_lobster2" min="0" style="width:40px;"/></td> 
                     <td class="price">$1900</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div id="drink_show">
            <h3>Bibite</h3>
            <table>
               <thead>
                  <tr>
                     <th></th>
                     <th></th>
                     <th>Cantidad</th>
                     <th>Grande</th>
                     <th>Cantidad</th>
                     <th>Chica</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="name">Agua</td>
                     <td class="composition"></td>
                      <td><input type="number" name="quantity" id="qt_water_G" min="0" style="width:40px;"/></td>
                     <td class="price">$40</td>
                       <td><input type="number" name="quantity" id="qt_water_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$15</td>
                  </tr>

                  <tr>
                     <td class="name">Refrescos</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_sodas" min="0" style="width:40px;"/></td> 
                     <td class="price">$15</td>
                  </tr>

                  <tr>
                     <td class="name">Malteadas</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_milkshake" min="0" style="width:40px;"/></td> 
                     <td class="price">$38</td>
                  </tr>

                  <tr>
                     <td class="name">Smoothies</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_smoothies" min="0" style="width:40px;"/></td> 
                     <td class="price">$35</td>
                  </tr>
               </tbody>
            </table>
            <table>
               <thead>
                  <tr>
                     <th>Caffé</th>
                     <th>Ingrediente</th>
                     <th>Cantidad</th>
                     <th>Precio</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="name">Expresso</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_expresso" min="0" style="width:40px;"/></td> 
                     <td class="price">$15</td>
                  </tr>

                  <tr>
                     <td class="name">Americano</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_american" min="0" style="width:40px;"/></td> 
                     <td class="price">$15</td>
                  </tr>

                  <tr>
                     <td class="name">Capuccino</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_capuccino" min="0" style="width:40px;"/></td> 
                     <td class="price">$20</td>
                  </tr>

                  <tr>
                     <td class="name">De sabor</td>
                     <td class="composition">Moka, vanilla, irish cream o amaretto</td>
                       <td><input type="number" name="quantity" id="qt_capuccinof" min="0" style="width:40px;"/></td> 
                     <td class="price">$25</td>
                  </tr>

                  <tr>
                     <td class="name">Chocolate caliente</td>
                     <td class="composition"></td>
                       <td><input type="number" name="quantity" id="qt_hotch" min="0" style="width:40px;"/></td> 
                     <td class="price">$25</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div id="dessert_show">
            <h3>Desserts</h3>
            <table>
               <thead>
                  <tr>
                     <th>Dolci</th>
                     <th>Ingrediente</th>
                     <th>Cantidad</th>
                     <th>Precio</th>
                  </tr>
               </thead>
               <tbody>
                 <tr>
                     <td class="name">Pizza de Nutella</td>
                     <td class="composition">(22 cm.) con frutas rojas o helado</td>
                       <td><input type="number" name="quantity" id="qt_nutpizza" min="0" style="width:40px;"/></td> 
                     <td class="price">$60</td>
                  </tr>
               </tbody>
            </table>
            <table>
               <thead>
                  <tr>
                     <th>Gelati</th>
                     <th></th>
                     <th>Cantidad</th>
                     <th>Sencillo</th>
                     <th>Cantidad</th>
                     <th>Doble</th>
                  </tr>
               </thead>
               <tbody>
                   <tr>
                     <td class="name">Helados del dia</td>
                     <td class="composition"></td>
                      <td><input type="number" name="quantity" id="qt_ice_G" min="0" style="width:40px;"/></td>
                     <td class="price">$40</td>
                       <td><input type="number" name="quantity" id="qt_ice_P" min="0" style="width:40px;"/></td> 
                     <td class="price">$25</td>
                  </tr>
               </tbody>
            </table>
         </div>
         <br />
         <br />
         <br />
      </section>
      <section id="Find_us" style="text-align:center">
         <div class="trait_dessus"></div>
         <h1 id="Find_us_map">Dirección</h1>
         <p style="display:inline-block" >
         <div id="googleMap" class="imageflottante"></div>
         <div id="localisation">
            <br>
            Plaza Tec<br />
            Av. Epigmiento Gonzalez<br />
            1-A Local 7 Qro<br />
         </div>
      </section>
      <section id="Opening_hours" style="text-align:center">
         <div class="trait_dessus"></div>
         <h1>Horario de apertura</h1>
         <div style="display:inline-block">
            <img src="images/devant.jpg" id="devant" alt="devanture"/>
            <p id="horaires">
               Le damos la bienvenida todos los días<br />
               Lunes - Sábado: 12:30 - 22:30<br />
               Domingo: 13:30 - 22:00<br />
            </p>
         </div>
           </section>
      <section id="contact">
         <div class="trait_dessus"></div>
         <h1 id="Contact">Contact</h1>
         <div style="display:inline-block">
            <div id="contact_number">
               <h3> Biagio Mangoni</h3>
               <ul>
                  <li><img id="whatsapp" alt="whatsapp" src="images/whatsapp_logo.png"> 442 172 0457 </li>
                  <li><img id="phone" alt="phone" src="images/phone.png"> 442 404 0404 <img id="parallavar" src="images/parallavar.png" alt="parallavar"></li>
               </ul>
            </div>
            <iframe id="facebook_feed" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgelato.biagio%2F%3Ffref%3Dts&tabs=timeline&width=450&height=600&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="450" height="600" style="overflow:hidden"></iframe>
            <h3> Mándanos un mensaje: </h3>
            <div id="contact_form">
               Asunto*: <input id="object" type="text"/> <br /><br />
               Mensaje*: <input id="message" type="text"/> <br /><br />
               <button onclick="get_submit()" id="button_send"> Enviar </button><br />
               *Campo obligatorio
            </div>
         </div>
      </section>
      <footer id="footer">
          <p id="signature">© 2016 Pizza Tec Querétaro.</p>
          <p>Sitio realizado por "La team des souleveurs": Eva Lopez, Guillaume Pivette, Victor Pouedras and François Sarfati,</p>
          <p>Estudiantes internacionales en el Tec de Monterrey - Campus Querétaro.</p>
		</footer>
   </body>
</html>