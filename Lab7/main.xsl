<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>Photography Collection</h2>
    <table border="2">
      <tr bgcolor="#662266">
        <th style="text-align:left">Camera</th>
        <th style="text-align:left">Price</th>
        <th style="text-align:left">Recommended for</th>
        <th style="text-align:left">DPReview Rating</th>
        <th style="text-align:left">Sensor</th>
        <th style="text-align:left">ISO</th>
        <th style="text-align:left">Max Shutter Speed</th>
        <th style="text-align:left">Battery Life</th>
        <th style="text-align:left">Weight and Dimensions (inc. batteries)</th>
        <th style="text-align:left">Video and Extras</th>
      </tr>
      <xsl:for-each select="photography/camera">
      <tr>
        <td><xsl:value-of select="name"/></td>
        <td><xsl:value-of select="price"/></td>
        <td><xsl:value-of select="recommended"/></td>
        <td><xsl:value-of select="rating"/></td>
        <td><xsl:value-of select="sensor"/></td>
        <td><xsl:value-of select="iso"/></td>
        <td><xsl:value-of select="mss"/></td>
        <td><xsl:value-of select="battery"/></td>
        <td><xsl:value-of select="weight"/></td>
        <td><xsl:value-of select="video"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

