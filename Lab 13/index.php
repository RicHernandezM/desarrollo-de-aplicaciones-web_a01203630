<?php
include 'header.html';
?>

<script>
function setId() {
  document.getElementById('body').setAttribute('id','contacto');
}

window.onload = setId;
</script>

<div class="mainImgTxt">
    <h1>
      <div id="contactoMainImg">
      <span class="latoBold" style="font-size:40px;">CONTACTO</span>
      </div>
    </h1>
</div>

<a href="#con"><button class="arrow center animated bounce infinite">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" height="40px" viewBox="0 0 80 80" xml:space="preserve">
    <polyline fill="none" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="
	60,30 40,55 20,30"/>
    <circle cx="40" cy="40" r="40" stroke="#FFFFFF" stroke-width="3" fill-opacity="0"
  </svg>    
</button>
</a>
    
  <div class="row" id="con">
    <div class="col-lg-12">
      <p class="openSansBold" style="font-size:50px; text-align: center;">
        <br>COMITÉ DIRECTIVO<br></p>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-4">
      <div class="threeContact">
        <p class="blanco lato" id="largeCuadro2" class="threeContact">
          <strong>SECRETARIO</strong><br>
        </p>
        <p class="latoLight normSize">
            <span style="color:#ffa326;">Miguel Reséndiz Fernández</span><br><br>
          <a href="mailto:mresendiz@rescate1.org" style="color:white;">mresendiz@rescate1.org</a>
        </p>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="threeContact"id="middleTreeContact">
        <p class="blanco lato" id="largeCuadro2" style="color:white;">
            <strong>PRESIDENTE</strong><span style="font-size:35px;color:white;"> y</span> Comandante<br></p>
        <p class="latoLight normSize">
          Manuél Malagón Anguiano<br><br>
          <a href="mailto:mmalagon@rescate1.org" style="color:white;">mmalagon@rescate1.org</a><br>
          Teléfono: (442)4115039<br>
          Nextel ID: 62*11*72147
        </p>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="threeContact">
        <p class="blanco lato" id="largeCuadro2">
          <strong>TESORERA</strong><br></p>
        <p class="latoLight normSize" >
            <span style="color:#ffa326;">Ma. Elena Unzueta Rosete</span><br><br>
          <a href="mailto:elena.unzueta@rescate1.org" style="color:white;">elena.unzueta@rescate1.org</a>
        </p>
    </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <p class="openSansBold" style="font-size:50px; text-align: center;">
        <br>NUESTRA UBICACIÓN<br></p>
    </div>
    <div class="col-lg-12"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.710975786465!2d-100.4007754135552!3d20.6406344077565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35a572b952363%3A0xe3681377d0daf8cd!2sRescate+1+de+Quer%C3%A9taro+I.A.P.!5e0!3m2!1ses!2sus!4v1477534220846" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe></div>
  </div>

<?php
include 'footer.html';
?>