require([
    'dojo/dom',
    'dojo/fx',
    'dojo/domReady!'
], function (dom, fx) {
    // The piece we had before...
    var greeting = dom.byId('greeting');

    // ...but now, with an animation!
    fx.slideTo({
        node: greeting,
        top: 0,
        left: 330
    }).play();
});