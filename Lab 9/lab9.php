<!doctype html>
<html>
    <head>
        
        <script src="js/scripts.js"></script>
        <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Lab 6</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/main.css">
    </head>
    
    <body>
          
<div id="third">
        <h1>Subscription Form</h1>
        <h4>* required information</h4>
        <form method="POST" action="validation.php">
            <p>First Name: <span class="error">* <?php echo $fnameErr;?></span><input type="text" name="fname" value=""/></p>
            <p>Last Name: <span class="error">* <?php echo $lnameErr;?></span><input type="text" name="lname" value=""/></p>
            <p>E-mail: <span class="error">* <?php echo $mailErr;?></span><input type="email" name="mail" value=""/></p>
            <p>Confirm E-mail: <span class="error">* <?php echo $mailErr;?></span><input type="email" name="mailc" value=""/></p>
            <p><input type="submit" class="expanded button" name="go" id="go"/></p>
        </form>
<br>
</div>
        
        <div id="input">             
<?php
echo "<h2>Your Input:</h2>";
echo "Your First Name is <strong>";
echo $fname;
echo "</strong><br>";
echo "Your Last Name is <strong>";
echo $lname;
echo "</strong><br>";
echo "Your E-mail is <strong>";
echo $mail;
echo "</strong><br>";
?>
        </div>

        
    <div id="watermark">LAB 9</div>
        
<!--Questions -->        
    <h1>Questions to answer</h1>    
        <p id="questions">Why is it good practice to separate the "view" from the "control"?<strong> To mantain a control over the code, keep an order, avoid long code pages; the idea is to get and calculate data as it is necessary.</strong>
            <br>
Apart from the $ _POST and $ _GET arrays, what other predefined arrays are in php and what are their functions?<strong> $_COOKIE: An associative array type passed to the current script via HTTP Cookies. $_REQUEST: An associative array that by default contains the contents of $_GET, $ _POST and $ _COOKIE.</strong>
            <br>
Explore the php functions, and describe 2 that you haven't seen in another languages and call your attention.<strong> Cookies: With PHP, you can both create and retrieve cookie values, which are often used to identify a user. Retreiving values for the user is the modern way  Include: This function allows to take all the text/code/markup that exists in the specified file and copies it into the file that uses the include statement.</strong>
        </p>
        
        
    </body>
    
</html>

