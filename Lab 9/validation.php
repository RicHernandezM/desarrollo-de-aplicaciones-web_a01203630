<?php

$fnameErr = $lnameErr = $mailErr = $mailcErr = "";
$fname = $lname = $mail = $mailc = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["fname"])) {
    $fnameErr = "First Name is required";
  } else {
    $fname = test_input($_POST["fname"]);
      if (!preg_match("/^[a-zA-Z ]*$/",$fname)) {  
      $fnameErr = "Only letters and white space allowed";
  }
  }

  if (empty($_POST["lname"])) {
    $lnameErr = "Last Name is required";
  } else {
    $lname = test_input($_POST["lname"]);
      if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
      $lnameErr = "Only letters and white space allowed";
  }
  }

  if (empty($_POST["mail"])) {
    $mailErr = "E-mail is required";
  } else {
    $mail = test_input($_POST["mail"]);
      if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
      $mailErr = "Invalid E-mail format";
  }
  }

  if (empty($_POST["mailc"])) {
    $mailc = "";
  } else {
    $mailc = test_input($_POST["mailc"]);
      if ($mail != $mailc){
        $mailcErr = "Enter the same mail written above";
        echo '<script language="javascript">';
          echo 'alert("Enter the same mail written above")';
          echo '</script>';
        
      }
  }

}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
include('lab9.php');

?>