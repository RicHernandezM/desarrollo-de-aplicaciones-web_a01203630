<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="main.css">
        <link type="text/css" rel="stylesheet" href="flags/flags.css">
              <title>Lab 8</title>
    </head>
    
<body>
    <h1>Average</h1>
    <p>Enter three numbers and the program will give you the average of the numbers.</p>
    <form method="get">
Number 1: <input type="number" name="1"><br>
Number 2: <input type="number" name="2"><br>
Number 3: <input type="number" name="3"><br>
        <input type="submit">
    </form>
        <p>Average: <?php $a = ($_GET["1"] + $_GET["2"] + $_GET["3"]) / 3; echo $a;
        ?></p>

<h1>Median</h1>
    <p>Enter five numbers and the program will give you the median of the numbers.</p>
    <form method="get">
Number 1: <input type="number" name="4"><br>
Number 2: <input type="number" name="5"><br>
Number 3: <input type="number" name="6"><br>
Number 4: <input type="number" name="7"><br>
Number 5: <input type="number" name="8"><br>
        <input type="submit">
    </form>
        <p>Median: <?php 
    $a = array($_GET["4"], $_GET["5"], $_GET["6"], $_GET["7"], $_GET["8"]);
    sort($a);
    echo $a[2]; 
        ?></p>
    
<h1>Average, Median, Order</h1>
    <p>Enter five numbers and the program will give you the average, median and the list of numbers sort from low to hi and viceversa.</p>
    <form method="get">
Number 1: <input type="number" name="9"><br>
Number 2: <input type="number" name="10"><br>
Number 3: <input type="number" name="11"><br>
Number 4: <input type="number" name="12"><br>
Number 5: <input type="number" name="13"><br>
        <input type="submit">
    </form>
        <?php 
    $b = array($_GET["9"], $_GET["10"], $_GET["11"], $_GET["12"], $_GET["13"]);
    sort($b);
    
        ?>
    <p>Average: <?php $c = ($_GET["9"] + $_GET["10"] + $_GET["11"] + $_GET["12"] + $_GET["13"]) / 5; echo $c; ?> </p>
    <p>Median: <?php echo $b[2]; ?> </p>
    <p>Order (low to high): <?php $arrlength=count($b);
            for($x=0;$x<$arrlength;$x++)
            {
                echo $b[$x];
            } ?> </p>
    <p>Order (high to low): <?php $arrlength2=-1;
            for($y=4;$y>$arrlength2;$y--)
            {
                echo $b[$y];
            } ?> </p>


<h1>Squares & Cubes</h1>
    <p>Enter four numbers and the program will give you the cube and square of the numbers.</p>
    <form method="get">
       <table align="center">
        <tr>
            <th>Enter number</th>
            <th>Cube</th>
            <th>Square</th>
        </tr>
        <tr>
            <td><input type="number" name="14"></td>
            <td><?php echo pow($_GET[14], 2); ?></td>
            <td><?php echo pow($_GET[14], 3); ?></td>
        </tr>
        <tr>
            <td><input type="number" name="15"></td>
            <td><?php echo pow($_GET[15], 2); ?></td>
            <td><?php echo pow($_GET[15], 3); ?></td>
        </tr>
        <tr>
            <td><input type="number" name="16"></td>
            <td><?php echo pow($_GET[16], 2); ?></td>
            <td><?php echo pow($_GET[16], 3); ?></td>
        </tr>
        <tr>
            <td><input type="number" name="17"></td>
            <td><?php echo pow($_GET[17], 2); ?></td>
            <td><?php echo pow($_GET[17], 3); ?></td>
        </tr>
        <tr>
            <td><input type="submit"></td>
        </tr>
        </table>
    </form>
    
<h1>Currency Calculator</h1>
    <p>Find the latest currency exchange rate.</p>
    <form method="get">
        <p><img class="flag flag-us" /> US Dollars: <input type="number" name="currency"></p><p><img class="flag flag-mx"/> Mexican pesos: <?php echo ($_GET[currency] * 19.295001); ?></p>
        <input type="submit">
    </form>
    
    <h3>Questions to answer</h3>
        <p>
            What does the phpinfo() function do? Describe and discuss 3 data that catch your attention. <strong>The phoinfo() function allows you to view the current PHP information for your server, such as: PHP version (which may be good to know for developing new php functions), Server information (which may let you know if a PHP update can be made), HTTP headers (which allow the browser to comunicate the server). </strong>
            What changes should you do in the server configuration so that it could be fit in a production environment? <strong> Change the IP adress, configure the DNS and the port that´s going to be used. </strong>
            What does it mean that the file with the html code that is deployed on the client side, runs on server side? Explain. <strong>It means that the code wrote in php is handled at the server, making a dynamic environment. This reduces the amount of bugs or campatibility issues.</strong>
        </p>
    
</body>
</html>
